unit Unit2;

// made sure to decouple GUI from logic
interface

uses
  uCDmongo,Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Phys.MongoDBDef,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  Vcl.StdCtrls, Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  FireDAC.Phys.MongoDBDataSet, FireDAC.Phys, FireDAC.Phys.MongoDB,
  FireDAC.UI.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async,
  System.Rtti, System.JSON.Types, System.JSON.Readers, System.JSON.BSON,
  System.JSON.Builders, FireDAC.Phys.MongoDBWrapper, FireDAC.VCLUI.Wait,
  FireDAC.Comp.UI, Vcl.ExtCtrls, Vcl.ComCtrls, Vcl.Grids, Vcl.DBGrids,
  IPPeerClient, REST.Backend.ServiceTypes, REST.Backend.MetaTypes, System.JSON,
  REST.Backend.ParseServices, REST.Backend.Providers,
  REST.Backend.ServiceComponents, REST.Backend.ParseProvider;

type
  TForm2 = class(TForm)
    Panel1: TPanel;
    Button4: TButton;
    Button1: TButton;
    Button2: TButton;
    Button5: TButton;
    Button3: TButton;
    TimerPull: TTimer;
    StatusBar1: TStatusBar;
    TimerPush: TTimer;
    Shape1: TShape;
    btnPushtmr: TButton;
    Button7: TButton;
    btnPulltmr: TButton;
    Shape2: TShape;
    btnPullDevice: TButton;
    btnPushDevice: TButton;
    FDPhysMongoDriverLink1: TFDPhysMongoDriverLink;
    pnlStore: TPanel;
    edtenterprise_name: TEdit;
    edtStroeName: TEdit;
    edtCollection_Name: TEdit;
    DBGrid4: TDBGrid;
    DSstore: TDataSource;
    shp1: TShape;
    lblMlab: TLabel;
    lblBaaS: TLabel;
    ParseProvider1: TParseProvider;
    BackendStorage1: TBackendStorage;
    edtBaseUrl: TEdit;
    btnConnectBaaS: TButton;
    rbMlab: TRadioButton;
    rbBaaS: TRadioButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Button5Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure btnPushtmrClick(Sender: TObject);
    procedure btnPulltmrClick(Sender: TObject);
    procedure btnPushDeviceClick(Sender: TObject);
    procedure btnConnectBaaSClick(Sender: TObject);
  private
    procedure PullValues();
    procedure DoCloseForm;
    procedure SetStore;
    procedure PushValues;
    procedure PushDevice;
    { Private declarations }
  public

    vCollection_Name: string;
    // I did not made it a const becouse may change in program itself
    vEnterpriseID: string;
    vStoreID: string;
    { Public declarations }
  end;

var
  Form2: TForm2;

const
  DB_NAME =  'computest' ; // 'compudime'; //'compuTest';
  //DB_NAME =  'compudime';

  CompuDashVer = '0.1';

implementation

{$R *.dfm}
//uses
//uCdMongo;

procedure TForm2.btnConnectBaaSClick(Sender: TObject);
begin
  ParseProvider1.BaseURL := edtBaseUrl.Text ;
end;

procedure TForm2.btnPulltmrClick(Sender: TObject);
begin
  TimerPull.Enabled := true ;
  btnPulltmr.Enabled := False ;
end;

procedure TForm2.btnPushDeviceClick(Sender: TObject);
begin
   PushDevice ;
end;

procedure TForm2.btnPushtmrClick(Sender: TObject);
begin
TimerPush.Enabled := True ;
btnPushtmr.Enabled := False ;
end;

procedure TForm2.Button1Click(Sender: TObject);
begin
 if NOT cdMongo.FDConnection.Connected then
  begin
    cdMongo.DoConnect(DB_NAME, vCollection_Name);
  end;
end;

procedure TForm2.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  DoCloseForm;
end;

procedure TForm2.FormShow(Sender: TObject);
begin
   { TODO : Set Panel1.visible only if entered program with specal pram }
  Panel1.Visible := True;
  SetStore;
  PullValues;
  PushValues;
  { TODO : Setup timers }
  btnPulltmr.Enabled := (not TimerPull.Enabled) ;
  btnPushtmr.Enabled := (not TimerPush.Enabled) ;

end;



procedure TForm2.SetStore;
begin

  vCollection_Name := edtCollection_Name.Text ;
  vEnterpriseID := edtenterprise_name.Text ;
  vStoreID      := edtStroeName.Text ;

end;

procedure TForm2.PullValues;
var
  aValue: string;
begin

  if NOT cdMongo.FDConnection.Connected then
  begin
    cdMongo.DoConnect(DB_NAME, vCollection_Name);
    SetStore ;
  end;

  // MyValueGet() should be used if a empty KEY should not be created

  cdMongo.MyValueSetGet(vCollection_Name, vEnterpriseID, vStoreID, 'compudash_ver',
    CompuDashVer);

   aValue := FormatDateTime('yyyy-mm-dd  hh:nn:ss',now) ;
   cdMongo.MyValueSetGet(vCollection_Name, vEnterpriseID, vStoreID,
    'Updated', aValue);



  aValue := 'My Store'; // Tis01_Site_Name
  cdMongo.MyValueSetGet(vCollection_Name, vEnterpriseID, vStoreID,
    'Tis01_Site_Name', aValue);

  aValue := 'My Store'; // TisWin3_Site_Name
  cdMongo.MyValueSetGet(vCollection_Name, vEnterpriseID, vStoreID,
    'TisWin3_Site_Name', aValue);

  aValue := '718-782-5555'; // Tis01_Site_Tel
  cdMongo.MyValueSetGet(vCollection_Name, vEnterpriseID, vStoreID,
    'Tis01_Site_Tel', aValue);

  aValue := '2019-12-05'; // Tis01_DTOS
  cdMongo.MyValueSetGet(vCollection_Name, vEnterpriseID, vStoreID,
    'Tis01_DTOS', aValue);
  cdMongo.MyValueSetGet(vCollection_Name, vEnterpriseID, vStoreID, 'Tis01_DTOS_push');

  aValue := '8.9.0.5555'; // Tis01_ver
  cdMongo.MyValueSetGet(vCollection_Name, vEnterpriseID, vStoreID, 'Tis01_ver', aValue);

  aValue := '1.4.0.25'; // TisWin3_ver
  cdMongo.MyValueSetGet(vCollection_Name, vEnterpriseID, vStoreID, 'TisWin3_ver', aValue);

  aValue := '1.189';
  cdMongo.MyValueSetGet(vCollection_Name, vEnterpriseID, vStoreID, 'db_ver', aValue);

  // Tis01_Modules
  if 'DEBIT' = 'DEBIT' then // only set if true
    cdMongo.MyValueSetGet(vCollection_Name, vEnterpriseID, vStoreID,
      'Tis01_Module.' + 'DEBIT', 'T');
  cdMongo.MyValueSetGet(vCollection_Name, vEnterpriseID, vStoreID,
    'Tis01_Module.' + 'UPSSHIP', 'T');
  cdMongo.MyValueSetGet(vCollection_Name, vEnterpriseID, vStoreID,
    'Tis01_Module.' + 'DLVCNTR', 'T');
  cdMongo.MyValueSetGet(vCollection_Name, vEnterpriseID, vStoreID,
    'Tis01_Module.' + 'DLABEL', 'T');


  cdMongo.mqData.Close ;
  cdMongo.mqData.Open ;
end;

procedure TForm2.PushValues;
var
  aValue: string;
begin
  if NOT cdMongo.FDConnection.Connected then
  begin
    cdMongo.DoConnect(DB_NAME, vCollection_Name);
    SetStore;
  end;

  aValue := cdMongo.MyValueSetGet(vCollection_Name, vEnterpriseID, vStoreID,
    'dtos_push');
  if aValue > '' then
    ShowMessage(aValue);

  cdMongo.mqData.Close;
  cdMongo.mqData.Open;
end;

procedure TForm2.Button2Click(Sender: TObject);
begin
  PullValues;
end;

procedure TForm2.Button3Click(Sender: TObject);
begin
  DoCloseForm;
end;

procedure TForm2.Button4Click(Sender: TObject);
begin
  SetStore;
end;

procedure TForm2.Button5Click(Sender: TObject);
begin
  PushValues;
end;

procedure TForm2.Button7Click(Sender: TObject);
begin
TimerPull.Enabled := False ;
TimerPush.Enabled := False ;
btnPushtmr.Enabled := True ;
btnPulltmr.Enabled := true ;
end;

procedure TForm2.DoCloseForm();
begin
  cdMongo.DoClose;
end;

//
procedure TForm2.PushDevice;
var
  aValue,vDevice_id : string;
begin

  vCollection_Name:='linebuster' ;

  if NOT cdMongo.FDConnection.Connected then
  begin
    cdMongo.DoConnect(DB_NAME, vCollection_Name);
  end;
  vDevice_id:='123456' ;
  // MyValueGet() should be used if a empty KEY should not be created


   cdMongo.DeviceSetGet(vCollection_Name, vDevice_id, 'buster_ver', '1.2');

   aValue := FormatDateTime('yyyy-mm-dd  hh:nn:ss',now) ;
   cdMongo.DeviceSetGet(vCollection_Name, vDevice_id, 'last_push', aValue);
   cdMongo.DeviceSetGet(vCollection_Name, vDevice_id, 'enterprise');
   cdMongo.DeviceSetGet(vCollection_Name, vDevice_id, 'store');




  cdMongo.mqData.Close ;
  cdMongo.mqData.Open ;
end;



end.
