unit uCdMongo;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Phys.MongoDBDef, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.UI.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.MongoDB,
  System.Rtti, System.JSON.Types, System.JSON.Readers, System.JSON.BSON,
  System.JSON.Builders, FireDAC.Phys.MongoDBWrapper, FireDAC.VCLUI.Wait,
  FireDAC.Comp.UI, Data.DB, FireDAC.Comp.Client, FireDAC.Comp.DataSet,
  FireDAC.Phys.MongoDBDataSet;

type
  TcdMongo = class(TDataModule)
    FDPhysMongoDriverLink1: TFDPhysMongoDriverLink;
    mqData: TFDMongoQuery;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    FDConnection: TFDConnection;
  private
    FCollection_Name: string;
    FDB_Name: string;
    FMongoConn: TMongoConnection;
    FEnv: TMongoEnv;

    procedure StartConnection;

    property DB_Name: string read FDB_Name;
    property Collection_Name: string read FCollection_Name;
  public
    procedure DoClose;

    function DeleteKey(const ACollection, AEnterprise_ID, AStore_ID,
      AmyKey: String): Boolean;
   procedure DoConnect(const ADB_Name, ACollection_Name: string);

   function MyValueSetGet(const ACollection, AEnterprise_ID, AStore_ID,
  AmyKey, AmyValue: String): String; Overload;

  function MyValueSetGet(const ACollection, AEnterprise_ID, AStore_ID,
  AmyKey: String): String; Overload;

  function DeviceSetGet(const ACollection, ADevice_ID, AmyKey,
      AmyValue: String): String; overload;

function DeviceSetGet(const ACollection, ADevice_ID,
   AmyKey: String): String; overload;

function MyValueGet(const ACollection, AEnterprise_ID, AStore_ID,
  AmyKey: String): String;

  end;

var
  cdMongo: TcdMongo;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TDataModule3 }

function TcdMongo.DeleteKey(const ACollection, AEnterprise_ID, AStore_ID,
  AmyKey: String): Boolean;
var
  MongoUpdate: TMongoUpdate;
  MongoColl: TMongoCollection;
begin
  //Result := false;
  {
    if not FDConnection.Connected then begin
    ShowMessage('Not connected to remote MongoDB');
    Exit;
    end;
    if (ACollection.Length = 0) or (AEnterprise_ID.Length = 0) or (AStore_ID.Length = 0) or (AmyKey.Length = 0) then begin
    ShowMessage('Have invalid some input param(s)');
    Exit;
    end;
  }
  MongoColl := FMongoConn.Collections[DB_NAME, ACollection];
  MongoUpdate := TMongoUpdate.Create(FEnv);
  try
    MongoUpdate.Match().Add('enterprise_name', AEnterprise_ID);
    MongoUpdate.Match().Add('store_name', AStore_ID);
    MongoUpdate.Modify().Unset().Field(AmyKey);
    MongoColl.Update(MongoUpdate);
    Result := True;
  finally
    FreeAndNil(MongoUpdate);
  end;
end;

procedure TcdMongo.DoClose;
begin
  mqData.Close;
  FDConnection.Connected := True;
end;

procedure TcdMongo.DoConnect(const ADB_Name, ACollection_Name: string);
var aServer,aUser,aPassword,aPort: string;
begin

  aServer:='ds131942.mlab.com';
  aUser:='compuTest'     ;
  aPassword:= 'compuTest123';
  aPort:='31942' ;

  //ADB_Name is also now set to test

  FDB_Name := ADB_Name;
  FCollection_Name := ACollection_Name;



  FDConnection.Params.Text := 'Server='+aServer.Trim+#10#13+
    'User_Name='+aUser+#10#13 + 'Password='+aPassword + #10#13 + 'DataBase=' +
    DB_NAME + #10#13 + 'Port='+aPort.Trim + #10#13 + 'DriverID=Mongo';


  FDConnection.Params.Values['MongoAdvanced'] := 'authenticationDatabase='
    + DB_NAME;
  StartConnection;
end;

function TcdMongo.MyValueSetGet(const ACollection, AEnterprise_ID,
  AStore_ID, AmyKey, AmyValue: String): String;
var
  MongoColl: TMongoCollection;
  MongoUpdate: TMongoUpdate;
begin
  {
    if not FDConnection.Connected then begin
    ShowMessage('Not connected to remote MongoDB');
    Exit;
    end;
    if (ACollection.Length = 0) or (AEnterprise_ID.Length = 0) or (AStore_ID.Length = 0) or (AmyKey.Length = 0) then begin
    ShowMessage('Have invalid some input param(s)');
    Exit;
    end;
  }

  MongoColl := FMongoConn.Collections[DB_NAME, ACollection];
  MongoUpdate := TMongoUpdate.Create(FEnv);
  try
    MongoUpdate.Match(Format('{"enterprise_name" : "%s", "store_name": "%s"}',
      [AEnterprise_ID, AStore_ID]));
    MongoUpdate.Modify().&Set().Field(AmyKey, AmyValue);
    MongoColl.Update(MongoUpdate, [TMongoCollection.TUpdateFlag.Upsert]);
  finally
    FreeAndNil(MongoUpdate);
  end;
  Result := AmyValue;
end;

function TcdMongo.MyValueGet(const ACollection, AEnterprise_ID, AStore_ID,
  AmyKey: String): String;
var
  Field: TField;
begin
  mqData.Close;
  mqData.CollectionName := ACollection;
  mqData.QMatch := Format('{"enterprise_name" : "%s", "store_name": "%s"}',
    [AEnterprise_ID, AStore_ID]);
  mqData.Open;
  Field := mqData.FindField(AmyKey);

  if not Assigned(Field) or (mqData.FieldByName('enterprise_name').AsString.Length = 0) then
    Result := ''
  else
    Result := Field.AsString;
end;

function TcdMongo.MyValueSetGet(const ACollection, AEnterprise_ID,
  AStore_ID, AmyKey: String): String;
var
  Field: TField;
begin
  {
    if not FDConnection.Connected then begin
    ShowMessage('Not connected to remote MongoDB');
    Exit;
    end;
    if (ACollection.Length = 0) or (AEnterprise_ID.Length = 0) or (AStore_ID.Length = 0) or (AmyKey.Length = 0) then begin
    ShowMessage('Have invalid some input param(s)');
    Exit;
    end;
  }

  mqData.Close;
  mqData.CollectionName := ACollection;
  mqData.QMatch := Format('{"enterprise_name" : "%s", "store_name": "%s"}',
    [AEnterprise_ID, AStore_ID]);
  mqData.Open;
  Field := mqData.FindField(AmyKey);
  if not Assigned(Field) or (mqData.FieldByName('enterprise_name')
    .AsString.Length = 0) then
    Result := MyValueSetGet(ACollection, AEnterprise_ID, AStore_ID, AmyKey, '')
  else
    Result := Field.AsString;
end;

procedure TcdMongo.StartConnection;
begin
  FDConnection.Connected := True;
  FMongoConn := TMongoConnection(FDConnection.CliObj);
  FEnv := FMongoConn.Env;

  mqData.DatabaseName := DB_Name;
  mqData.CollectionName := Collection_Name;
  mqData.Open;
end;
////


   function TcdMongo.DeviceSetGet(const ACollection, ADevice_ID,
   AmyKey, AmyValue: String): String;
var
  MongoColl: TMongoCollection;
  MongoUpdate: TMongoUpdate;
begin
  MongoColl := FMongoConn.Collections[DB_NAME, ACollection];
  MongoUpdate := TMongoUpdate.Create(FEnv);
  try
    MongoUpdate.Match(Format('{"device_id" : "%s"}',
      [ADevice_ID]));
    MongoUpdate.Modify().&Set().Field(AmyKey, AmyValue);
    MongoColl.Update(MongoUpdate, [TMongoCollection.TUpdateFlag.Upsert]);
  finally
    FreeAndNil(MongoUpdate);
  end;
  Result := AmyValue;
end;


function TcdMongo.DeviceSetGet(const ACollection, ADevice_ID,
   AmyKey: String): String;
var
  Field: TField;
begin

  mqData.Close;
  mqData.CollectionName := ACollection;
  mqData.QMatch := Format('{"device_id" : "%s"}',
    [ADevice_ID]);
  mqData.Open;
  Field := mqData.FindField(AmyKey);
  if not Assigned(Field) or (mqData.FieldByName('device_id')
    .AsString.Length = 0) then
    Result := DeviceSetGet(ACollection, ADevice_ID, AmyKey, '')
  else
    Result := Field.AsString;
end;



end.
