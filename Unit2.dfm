object Form2: TForm2
  Left = 0
  Top = 0
  Caption = 'Form2'
  ClientHeight = 478
  ClientWidth = 666
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 110
    Height = 453
    Align = alLeft
    TabOrder = 0
    object Shape1: TShape
      AlignWithMargins = True
      Left = 13
      Top = 289
      Width = 84
      Height = 1
      Margins.Left = 12
      Margins.Top = 12
      Margins.Right = 12
      Margins.Bottom = 12
      Align = alTop
      ExplicitTop = 314
    end
    object Shape2: TShape
      AlignWithMargins = True
      Left = 13
      Top = 171
      Width = 84
      Height = 1
      Margins.Left = 12
      Margins.Top = 12
      Margins.Right = 12
      Margins.Bottom = 12
      Align = alTop
      ExplicitLeft = 16
      ExplicitTop = 208
      ExplicitWidth = 65
    end
    object Button4: TButton
      AlignWithMargins = True
      Left = 7
      Top = 7
      Width = 96
      Height = 25
      Margins.Left = 6
      Margins.Top = 6
      Margins.Right = 6
      Align = alTop
      Caption = 'Set Store'
      TabOrder = 0
      OnClick = Button4Click
    end
    object Button1: TButton
      AlignWithMargins = True
      Left = 7
      Top = 38
      Width = 96
      Height = 25
      Margins.Left = 6
      Margins.Right = 6
      Align = alTop
      Caption = 'Connect'
      TabOrder = 1
      OnClick = Button1Click
    end
    object Button2: TButton
      AlignWithMargins = True
      Left = 7
      Top = 69
      Width = 96
      Height = 25
      Margins.Left = 6
      Margins.Right = 6
      Align = alTop
      Caption = 'Pull Values'
      TabOrder = 2
      OnClick = Button2Click
    end
    object Button5: TButton
      AlignWithMargins = True
      Left = 7
      Top = 100
      Width = 96
      Height = 25
      Margins.Left = 6
      Margins.Right = 6
      Align = alTop
      Caption = 'Push Values'
      TabOrder = 3
      OnClick = Button5Click
    end
    object Button3: TButton
      AlignWithMargins = True
      Left = 7
      Top = 131
      Width = 96
      Height = 25
      Margins.Left = 6
      Margins.Right = 6
      Align = alTop
      Caption = 'Close'
      TabOrder = 4
      OnClick = Button3Click
    end
    object btnPushtmr: TButton
      AlignWithMargins = True
      Left = 7
      Top = 218
      Width = 96
      Height = 25
      Margins.Left = 6
      Margins.Right = 6
      Align = alTop
      Caption = 'Start Push Timer'
      TabOrder = 5
      OnClick = btnPushtmrClick
    end
    object Button7: TButton
      AlignWithMargins = True
      Left = 7
      Top = 187
      Width = 96
      Height = 25
      Margins.Left = 6
      Margins.Right = 6
      Align = alTop
      Caption = 'Stop Timers'
      TabOrder = 6
      OnClick = Button7Click
    end
    object btnPulltmr: TButton
      AlignWithMargins = True
      Left = 7
      Top = 249
      Width = 96
      Height = 25
      Margins.Left = 6
      Margins.Right = 6
      Align = alTop
      Caption = 'Start Pull Timer'
      TabOrder = 7
      OnClick = btnPulltmrClick
    end
    object btnPullDevice: TButton
      AlignWithMargins = True
      Left = 7
      Top = 336
      Width = 96
      Height = 25
      Margins.Left = 6
      Margins.Right = 6
      Align = alTop
      Caption = 'Pull Device'
      TabOrder = 8
    end
    object btnPushDevice: TButton
      AlignWithMargins = True
      Left = 7
      Top = 305
      Width = 96
      Height = 25
      Margins.Left = 6
      Margins.Right = 6
      Align = alTop
      Caption = 'Push Device'
      TabOrder = 9
      OnClick = btnPushDeviceClick
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 459
    Width = 666
    Height = 19
    Panels = <>
    ExplicitWidth = 528
  end
  object pnlStore: TPanel
    Left = 119
    Top = 8
    Width = 153
    Height = 297
    TabOrder = 2
    object shp1: TShape
      AlignWithMargins = True
      Left = 4
      Top = 122
      Width = 145
      Height = 1
      Margins.Top = 9
      Align = alTop
      ExplicitLeft = 32
      ExplicitTop = 96
      ExplicitWidth = 81
    end
    object lblMlab: TLabel
      AlignWithMargins = True
      Left = 4
      Top = 10
      Width = 145
      Height = 13
      Margins.Top = 9
      Align = alTop
      Alignment = taCenter
      Caption = 'Mlab Settings'
      ExplicitWidth = 64
    end
    object lblBaaS: TLabel
      AlignWithMargins = True
      Left = 4
      Top = 135
      Width = 145
      Height = 13
      Margins.Top = 9
      Align = alTop
      Alignment = taCenter
      Caption = 'BaaS Setup'
      ExplicitWidth = 55
    end
    object edtenterprise_name: TEdit
      AlignWithMargins = True
      Left = 4
      Top = 89
      Width = 145
      Height = 21
      Align = alTop
      TabOrder = 0
      Text = 'MyEnterprise'
      TextHint = 'enterprise name'
      ExplicitLeft = 16
      ExplicitTop = 37
      ExplicitWidth = 121
    end
    object edtStroeName: TEdit
      AlignWithMargins = True
      Left = 4
      Top = 62
      Width = 145
      Height = 21
      Align = alTop
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Text = 'MySupperStore'
      TextHint = 'Store Name'
      ExplicitLeft = 16
      ExplicitTop = 64
      ExplicitWidth = 121
    end
    object edtCollection_Name: TEdit
      AlignWithMargins = True
      Left = 4
      Top = 35
      Width = 145
      Height = 21
      Margins.Top = 9
      Align = alTop
      TabOrder = 2
      Text = 'CompuDashMain'
      TextHint = 'Collection Name'
      ExplicitLeft = 16
      ExplicitTop = 10
      ExplicitWidth = 121
    end
    object edtBaseUrl: TEdit
      AlignWithMargins = True
      Left = 4
      Top = 157
      Width = 145
      Height = 21
      Margins.Top = 6
      Align = alTop
      TabOrder = 3
      Text = 'https://parseapi.back4app.com/classes/setup_test/'
      ExplicitLeft = 16
      ExplicitTop = 154
      ExplicitWidth = 121
    end
    object btnConnectBaaS: TButton
      Left = 16
      Top = 192
      Width = 113
      Height = 25
      Caption = 'Connect BaaS'
      TabOrder = 4
      OnClick = btnConnectBaaSClick
    end
    object rbMlab: TRadioButton
      Left = 40
      Top = 232
      Width = 113
      Height = 17
      Caption = 'Mlab'
      TabOrder = 5
    end
    object rbBaaS: TRadioButton
      Left = 40
      Top = 255
      Width = 113
      Height = 17
      Caption = 'BaaS'
      TabOrder = 6
    end
  end
  object DBGrid4: TDBGrid
    AlignWithMargins = True
    Left = 278
    Top = 3
    Width = 385
    Height = 453
    Align = alRight
    DataSource = DSstore
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object TimerPull: TTimer
    Interval = 100000
    Left = 264
    Top = 400
  end
  object TimerPush: TTimer
    Left = 352
    Top = 400
  end
  object FDPhysMongoDriverLink1: TFDPhysMongoDriverLink
    Left = 472
    Top = 408
  end
  object DSstore: TDataSource
    DataSet = cdMongo.mqData
    Left = 429
    Top = 116
  end
  object ParseProvider1: TParseProvider
    ApiVersion = '1'
    ApplicationID = 'K4RDNIGm8zium6uU1Le4xqhJ5lvA8yELHzDgkDvZ'
    RestApiKey = 'ZE3bbhXzSiXY2luGSX88ILxSh1hHeYQ3ihb3xg7Z'
    MasterKey = 'L3SFunJOZjgLqyMvaY9fPk0uJHqEBvkRF5jfRUGj'
    AndroidPush.InstallationID = '995AF4D5-7C36-4A4A-8B8D-6F2E5716BFA4'
    Left = 216
    Top = 272
  end
  object BackendStorage1: TBackendStorage
    Provider = ParseProvider1
    Left = 216
    Top = 336
  end
end
